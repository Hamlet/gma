--[[

	Copyright © 2018 Hamlet <hamlatmesehub@riseup.net>
	Copyright © 2016, 2018 azekillDIABLO, taikedz, Lone_Wolf

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Life timers
--

-- Used to despawn mobiles.
gma.TimerLifeCheck = function(self, dtime)
	if (self.gma_life_time > 0) then
		self.gma_life_time = self.gma_life_time - dtime
	else
		self.object:remove()
	end
end


-- Used to check if mobiles can breath.
gma.TimerBreathCheck = function(self, dtime)
	if (self.gma_counter_suffocation_check <= 0) then
		gma.CanBreath(self)
		self.gma_counter_suffocation_check = 2.0

	else
		self.gma_counter_suffocation_check =
			(self.gma_counter_suffocation_check - dtime)

	end
end


-- Used to allow to mobiles to recover breath points.
gma.TimerRecoverBreathCheck = function(self, dtime)
	if (self.gma_counter_recover_breath_check <= 0) then
		-- If the mobile has lost breath points.
		if (gma.BreathLevelCheck(self) == true) then
			gma.RecoverBreath(self)
		end

		self.gma_counter_recover_breath_check = 0.5

	else
		self.gma_counter_recover_breath_check =
			(self.gma_counter_recover_breath_check - dtime)

	end
end


-- Used to damage mobiles due to fire or lava.
gma.TimerBurnCheck = function(self, dtime)
	if (self.gma_counter_burn_check <= 0) then
		gma.BurnCheck(self)
		self.gma_counter_burn_check = 1.0

	else
		self.gma_counter_burn_check = (self.gma_counter_burn_check - dtime)

	end
end


-- Used to damage mobiles due to water.
gma.TimerHydroCheck = function(self, dtime)
	if (self.gma_counter_hydro_check <= 0) then
		gma.HydroCheck(self)
		self.gma_counter_hydro_check = 1.0

	else
		self.gma_counter_hydro_check = (self.gma_counter_hydro_check - dtime)

	end
end


-- Used to damage mobiles due to light or darkness.
gma.TimerLightCheck = function(self, dtime)
	if (self.gma_counter_light_check <= 0) then
		gma.LightLevelCheck(self)
		self.gma_counter_light_check = 1.0

	else
		self.gma_counter_light_check = (self.gma_counter_light_check - dtime)

	end
end


-- Used to allow to injuried mobiles to recover health.
gma.TimerRecoverHealthCheck = function(self, dtime)
	if (self.gma_counter_recover_health_check <= 0) then
		-- If the mobile is injuried.
		if (gma.HealthLevelCheck(self) == true) then
			gma.RecoverHealth(self)
		end

		self.gma_counter_recover_health_check = 4.0

	else
		self.gma_counter_recover_health_check =
			(self.gma_counter_recover_health_check - dtime)

	end
end


--
-- Movement timers
--

-- Used to track for how much time a mobile has been moving.
gma.TimerMovementCheck = function(self, dtime)
	if (self.gma_action == "moving")
	or (self.gma_action == "moving_fast")
	then
		self.gma_counter_movement = (self.gma_counter_movement + dtime)

		--[[
		print(self.gma_mobile_id .. " has been moving for " ..
			string.format("%.2f", self.gma_counter_movement) .. "secs")
		--]]

	else
		self.gma_counter_movement = 0.0

	end
end


-- Used to make mobiles fall.
gma.TimerGravityCheck = function(self, dtime)
	if (self.gma_counter_gravity_check <= 0) then
		gma.Gravity(self)
		self.gma_counter_gravity_check = 1.0

	else
		self.gma_counter_gravity_check = (self.gma_counter_gravity_check - dtime)

	end
end


-- Used to check whether if a mobile has fallen.
gma.TimerFallCheck = function(self, dtime)
	if (self.gma_counter_fall_check <= 0) then
		gma.FallCheck(self)
		self.gma_counter_fall_check = 0.5

	else
		self.gma_counter_fall_check = (self.gma_counter_fall_check - dtime)

	end
end


-- Used to check wheter if a mobile has to be damaged
-- due to a fall.
gma.TimerFallDamageCheck = function(self, dtime)
	if (self.gma_counter_fall_damage_check <= 0) then
		gma.FallDamageCheck(self)
		self.gma_counter_fall_damage_check = 0.5

	else
		self.gma_counter_fall_damage_check =
			(self.gma_counter_fall_damage_check - dtime)

	end
end


-- Used to check if mobiles are stuck.
gma.TimerStuckCheck = function(self, dtime)
	if (self.gma_counter_stuck_check <= 0) then
		local mobile_is_stuck = gma.CheckIfStuck(self)

		if (mobile_is_stuck ~= nil)
		and (mobile_is_stuck ~= false)
		then
			gma.Move(self)
			self.gma_counter_change_direction = 2.5
		end

		self.gma_counter_stuck_check = 5.0

	else
		self.gma_counter_stuck_check = (self.gma_counter_stuck_check - dtime)

	end
end


-- Used to make mobiles change their direction.
gma.TimerDirectionChange = function(self, dtime)
	if (self.gma_counter_change_direction <= 0) then

		if (self.gma_state_e ~= "engaged") then
			if (gma.StandOrMove(self) == false) then
				if (self.gma_animations_table ~= nil) then
					if (gma.AnimationCheck(self, 1) == false) then
						gma.ApplyAnimation(self, "standing")
					end
				end
				--print(self.gma_mobile_id .. " Timer - Stopping.")
				gma.Stop(self)
				self.gma_counter_change_direction =
					self.gma_direction_change_interval

			else
				if (self.gma_animations_table ~= nil) then
					if (gma.AnimationCheck(self, 4) == false) then
						gma.ApplyAnimation(self, "moving")
					end
				end

				--print(self.gma_mobile_id .. " Timer. - Moving.")
				gma.Move(self)
				self.gma_counter_change_direction =
					self.gma_direction_change_interval

			end

		else
			local mob_position = self.object:get_pos()
			local foe_position = self.gma_foe:get_pos()
			local range = self.gma_range_detect

			if (gma.CheckIfIntoRange(mob_position, foe_position, range) == true) then
				--print(self.gma_mobile_id .. " Timer - Moving attacking.")
				gma.MoveToTarget(self)
				self.gma_counter_change_direction = 0.025

			else
				if (gma.StandOrMove(self) == false) then
					if (self.gma_animations_table ~= nil) then
						if (gma.AnimationCheck(self, 1) == false) then
							gma.ApplyAnimation(self, "standing")
						end
					end
					--print(self.gma_mobile_id .. " Timer - Stopping.")
					gma.Stop(self)
					self.gma_counter_change_direction =
						self.gma_direction_change_interval

				else
					if (self.gma_animations_table ~= nil) then
						if (gma.AnimationCheck(self, 4) == false) then
							gma.ApplyAnimation(self, "moving")
						end
					end

					--print(self.gma_mobile_id .. " Timer. - Moving.")
					gma.Move(self)
					self.gma_counter_change_direction =
						self.gma_direction_change_interval

				end
			end
		end

	else
		self.gma_counter_change_direction =
			(self.gma_counter_change_direction - dtime)
	end
end


--
-- Combat timers
--

gma.TimerMeleeAttack = function(self, dtime)
	if (self.gma_counter_melee_attack <= 0) then
		gma.MeleeAttack(self)
		self.gma_counter_melee_attack = 1.0

	else
		self.gma_counter_melee_attack =
			(self.gma_counter_melee_attack - dtime)

	end
end
