--[[

	Copyright © 2018 Hamlet <hamlatmesehub@riseup.net>
	Copyright © 2016, 2018 azekillDIABLO, taikedz, Lone_Wolf

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


gma.OnStepDefault = function(self, dtime)

	-- Check whether if the mobile's direction has to be changed.
	gma.TimerDirectionChange(self, dtime)

	-- Used to track for how much time a mobile has been moving.
	--gma.TimerMovementCheck(self, dtime)

	-- Check whether if the mobile has to be removed.
	gma.TimerLifeCheck(self, dtime)

	-- Check whether if the mobile is stuck.
	gma.TimerStuckCheck(self, dtime)

	-- Check whether if the mobile must fall.
	if (self.weight ~= 0) then
		gma.TimerGravityCheck(self, dtime)
	end

	-- Check whether if the mobile has fallen.
	if (self.weight ~= 0) then
		gma.TimerFallCheck(self, dtime)
	end

	-- Check whether if the mobile has to suffer fall damage.
	if (self.gma_flaws_fall == true) then
		gma.TimerFallDamageCheck(self, dtime)
	end

	-- Check whether if the mobile is suffocating.
	if (self.gma_suffocation == true) then
		gma.TimerBreathCheck(self, dtime)
	end

	-- Check whether if the mobile has to suffer fire or lava damage.
	if (self.gma_flaws_fire == true) then
		gma.TimerBurnCheck(self, dtime)
	end

	-- Check whether if the mobile has to suffer water damage.
	if (self.gma_flaws_water == true) then
		gma.TimerHydroCheck(self, dtime)
	end

	-- Check whether if the mobile has to suffer light/darkness damage.
	if (self.gma_flaws_light == true) then
		gma.TimerLightCheck(self, dtime)
	end

	-- Check whether if the mobile has lost breath points and can
	-- recover them.
	if (self.gma_state_a ~= "suffocating") then
		gma.TimerRecoverBreathCheck(self, dtime)
	end

	-- Check whether if the mobile is injuried and can recover health.
	if (gma.AllowHealthRecovery(self) == true) then
		gma.TimerRecoverHealthCheck(self, dtime)
	end

	-- Check whether if the mobile can execute a melee attack.
	if (self.gma_action == "attacking") then
		gma.TimerMeleeAttack(self, dtime)
	end

end
