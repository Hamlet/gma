--[[

	Copyright © 2018 Hamlet <hamlatmesehub@riseup.net>
	Copyright © 2016, 2018 azekillDIABLO, taikedz, Lone_Wolf

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Mobile's sounds
gma.PlaySound = function(self, action)
	-- for backward compatibility: if the existing entities have been
	-- spawned without the "sounds" specification in the entity
	-- definition, the game will not crash.
	if (self.gma_sounds_table ~= nil) then
		local sounds_table = self.gma_sounds_table
		-- fetch the "sounds" specification

		local standing = sounds_table["standing"]
		-- fetch the values for the "standing" sound
		-- for when the mob stands still

		local sitting = sounds_table["sitting"]
		-- fetch the values for the "standing" sound
		-- for when the mob stands still

		local laying = sounds_table["laying"]
		-- fetch the values for the "standing" sound
		-- for when the mob stands still

		local moving = sounds_table["moving"]
		-- fetch the values for the "moving" sound
		-- for when the mob walks, swims or flies

		local moving_fast = sounds_table["moving_fast"]
		-- fetch the values for the "moving_fast" sound
		-- same as the previous, but for running or similar

		local jumping = sounds_table["jumping"]
		-- fetch the values for the "jumping" sound

		local attacking = sounds_table["attacking"]
		-- fetch the values for the "attacking" sound
		-- for when the mob bites, punches, digs, etc.

		local attacking_moving = sounds_table["attacking_moving"]
		-- fetch the values for the "attacking while moving" sound
		-- for when the mob bites, punches, digs, etc. while moving

		if (action ~= nil) then
		-- when calling the function you must specify which action
		-- is being done, else the function will abort.
			if (action == "standing") then

				-- Check whether if has been specified a sound.
				if (standing ~= nil) then

					-- Check whether if the sound should be played
					if (gma.ChanceCheck(sounds_table["standing"].a) == true)
					then
						minetest.sound_play(sounds_table["standing"].b, {
							object = self.object,
							gain = sounds_table["standing"].c
								or 1.0,

							max_hear_distance = sounds_table["standing"].d
								or 20,

							loop = sounds_table["standing"].e
								or false,
						})

						self.gma_current_sound = 1
					end
				end


			elseif (action == "sitting") then

				-- Check whether if has been specified a sound.
				if (sitting ~= nil) then

					-- Check whether if the sound should be played
					if (gma.ChanceCheck(sounds_table["sitting"].a) == true)
					then
						minetest.sound_play(sounds_table["sitting"].b, {
							object = self.object,
							gain = sounds_table["sitting"].c
								or 1.0,

							max_hear_distance = sounds_table["sitting"].d
								or 20,

							loop = sounds_table["sitting"].e
								or false,
						})

						self.gma_current_sound = 2
					end
				end


			elseif (action == "laying") then

				-- Check whether if has been specified a sound.
				if (laying ~= nil) then

					-- Check whether if the sound should be played
					if (gma.ChanceCheck(sounds_table["laying"].a) == true)
					then
						minetest.sound_play(sounds_table["laying"].b, {
							object = self.object,
							gain = sounds_table["laying"].c
								or 1.0,

							max_hear_distance = sounds_table["laying"].d
								or 20,

							loop = sounds_table["laying"].e
								or false,
						})

						self.gma_current_sound = 3
					end
				end


			elseif (action == "moving") then

				-- Check whether if has been specified a sound.
				if (moving ~= nil) then

					-- Check whether if the sound should be played
					if (gma.ChanceCheck(sounds_table["moving"].a) == true)
					then
						minetest.sound_play(sounds_table["moving"].b, {
							object = self.object,
							gain = sounds_table["moving"].c
								or 1.0,

							max_hear_distance = sounds_table["moving"].d
								or 20,

							loop = sounds_table["moving"].e
								or false,
						})

						self.gma_current_sound = 4
					end
				end


			elseif (action == "moving_fast") then

				-- Check whether if has been specified a sound.
				if (moving_fast ~= nil) then

					-- Check whether if the sound should be played
					if (gma.ChanceCheck(sounds_table["moving_fast"].a) == true)
					then
						minetest.sound_play(sounds_table["moving_fast"].b, {
							object = self.object,
							gain = sounds_table["moving_fast"].c
								or 1.0,

							max_hear_distance = sounds_table["moving_fast"].d
								or 20,

							loop = sounds_table["moving_fast"].e
								or false,
						})

						self.gma_current_sound = 5
					end
				end


			elseif (action == "jumping") then

				-- Check whether if has been specified a sound.
				if (jumping ~= nil) then

					-- Check whether if the sound should be played
					if (gma.ChanceCheck(sounds_table["jumping"].a) == true)
					then
						minetest.sound_play(sounds_table["jumping"].b, {
							object = self.object,
							gain = sounds_table["jumping"].c
								or 1.0,

							max_hear_distance = sounds_table["jumping"].d
								or 20,

							loop = sounds_table["jumping"].e
								or false,
						})

						self.gma_current_sound = 6
					end
				end


			elseif (action == "attacking") then

				-- Check whether if has been specified a sound.
				if (attacking ~= nil) then

					-- Check whether if the sound should be played
					if (gma.ChanceCheck(sounds_table["attacking"].a) == true)
					then
						minetest.sound_play(sounds_table["attacking"].b, {
							object = self.object,
							gain = sounds_table["attacking"].c
								or 1.0,

							max_hear_distance = sounds_table["attacking"].d
								or 20,

							loop = sounds_table["attacking"].e
								or false,
						})

						self.gma_current_sound = 7
					end
				end


			elseif (action == "attacking_moving") then

				-- Check whether if has been specified a sound.
				if (attacking_moving ~= nil) then

					-- Check whether if the sound should be played
					if (gma.ChanceCheck(sounds_table["attacking_moving"].a) == true)
					then
						minetest.sound_play(sounds_table["attacking_moving"].b, {
							object = self.object,
							gain = sounds_table["attacking_moving"].c
								or 1.0,

							max_hear_distance = sounds_table["attacking_moving"].d
								or 20,

							loop = sounds_table["attacking_moving"].e
								or false,
						})

						self.gma_current_sound = 8
					end
				end

			else
				print("H.M.M.: sound action \"" .. action ..
					"\" not supported or typed wrong.")
			end

		else
			print("H.M.M.: sound action's value is nil.")

		end
	end
end
