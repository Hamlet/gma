--[[

	Copyright © 2018 Hamlet <hamlatmesehub@riseup.net>
	Copyright © 2016, 2018 azekillDIABLO, taikedz, Lone_Wolf

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Used to set the mobile's foe and engaged state.
gma.OnPunchDefault = function(self, puncher, time_from_last_punch,
	tool_capabilities, dir)

	-- If the mobile is not passive
	if (self.gma_temper > 0) then
		if (self.gma_foe == nil) then
			self.gma_foe = puncher
		end

		if (self.gma_state_e ~= "engaged") then
			self.gma_state_e = "engaged"
			self.gma_counter_change_direction = 0.0
		end
	end
end


-- Used to check whether if the player is still online.
gma.FoeCheck = function(self)
	if (minetest.is_player(self.gma_foe) ~= true) then
		if (self.gma_state_e == "engaged") then
			self.gma_state_e = "normal"
			self.gma_counter_change_direction = 0.0
		end

		self.gma_foe = nil
	end
end


-- Used to punch the mobile's foe.
gma.MeleeAttack = function(self)
	local target = self.gma_foe
	local mobile = self.object
	local hp_to_subtract = 0

	if (self.gma_damage_variable == false) then
		hp_to_subtract = self.gma_damage_dealt

	else
		hp_to_subtract = math.random(self.gma_damage_min, self.gma_damage_max)

	end

	local weapon = {
		full_punch_interval = 1.0,
		max_drop_level = 0,
		groupcaps = {
			fleshy = {
				times = {
					[2] = 2.0,
					[3] = 1.0
				},
				uses = 0,
				maxlevel = 1
			},
		},
		damage_groups = {fleshy = hp_to_subtract},
	}

	target:punch(mobile, 1, weapon, nil)
end
