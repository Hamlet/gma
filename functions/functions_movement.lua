--[[

	Copyright © 2018 Hamlet <hamlatmesehub@riseup.net>
	Copyright © 2016, 2018 azekillDIABLO, taikedz, Lone_Wolf

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Choose whether if the mobile should stand still or move.
gma.StandOrMove = function(self)
	local chance = self.gma_mobility

	-- Force a default value if it has been entered an out of
	-- range one.
	if (chance == nil)
	or (chance < 0)
	or (chance > 100)
	then
		chance = 50
	end

	-- Stand still
	if (chance == 0) then
		return false
	end

	-- Keep moving
	if (chance == 100) then
		return true
	end

	local number = math.random(1, 100)

	-- E.g. chance = 10
	-- If number == 1,2,3,...,10 then move, else don't.
	if (number <= chance) then
		return true
	else
		return false
	end
end


-- If the mobile's weight is major than 0 then apply
-- a negative Y axis acceleration (+Y = up, -Y = down).
gma.Gravity = function(self)
	if (self.weight > 0) then
		self.object:set_acceleration({x = 0, y = -9.8062, z = 0})
	end
end


-- Apply a random direction to the mobile.
gma.Move = function(self)
	local direction = {z = 0, y = 0, x = 0}
	local axis_z = math.random(-1, 1)
	local axis_x = math.random(-1, 1)
	local mob_speed = 0

	if (self.gma_action ~= "moving_fast") then
		mob_speed = self.gma_speed

	else
		mob_speed = self.gma_fast_speed

	end

	-- Ensure that at least one of the variables is not 0,
	-- else the mobile would stop.
	if (axis_z == 0) and (axis_x == 0) then
		if (math.random(0, 1) == 0) then
			axis_z = 1
		else
			axis_x = -1
		end
	end

	direction = {z = axis_z, y = 0, x = axis_x}

	if (self.gma_action ~= "moving")
	or (self.gma_action ~= "moving_fast")
	then
		self.gma_action = "moving"
		gma.ApplyAnimation(self, "moving")
		gma.PlaySound(self, "moving")
	end

	--print(self.gma_mobile_id .. " is moving.")
	self.object:set_velocity(vector.multiply(direction, mob_speed))
end


-- Apply a direction that moves the mobile toward its target.
gma.MoveToTarget = function(self)
	local mob_position = self.object:get_pos()
	local foe_position = self.gma_foe:get_pos()
	local distance = vector.distance(mob_position, foe_position)

	-- Move the mobile within melee range, if it can see its target.
	if (distance > self.gma_range_melee) then
		if (gma.CheckLineOfSight(mob_position, foe_position, 1,
			self.gma_sight_offset) == true)
		then

			local direction = vector.direction(mob_position, foe_position)
			direction.y = 0 -- Else the mob could fly to its target.

			local mob_speed = self.gma_fast_speed

			if (self.gma_action ~= "moving_fast") then
				self.gma_action = "moving_fast"
				gma.ApplyAnimation(self, "moving_fast")
				gma.PlaySound(self, "moving_fast")
			end

			self.object:set_velocity(vector.multiply(direction, mob_speed))
		end


	-- If the mobile is standing still and it can see its target, then
	-- keep it facing its target while attacking.
	else
		if (gma.CheckLineOfSight(mob_position, foe_position, 1,
			self.gma_sight_offset) == true)
		then

			local direction = vector.direction(mob_position, foe_position)

			if (self.gma_action ~= "attacking") then
				self.gma_action = "attacking"
				gma.ApplyAnimation(self, "attacking")
				gma.PlaySound(self, "attacking")
			end

			self.object:set_velocity({x = 0, y = 0, z = 0})

			-- Handle the custom model rotation, if any.
			if (self.automatic_face_movement_dir ~= false) then
				-- Turn the custom model's rotation from degrees to radians
				local offset = math.rad(self.automatic_face_movement_dir)

				-- Rotate the mobile to face its target
				self.object:set_yaw(math.atan2(direction.z, direction.x) + offset)

			else
				self.object:set_yaw(math.atan2(direction.z, direction.x))

			end

		-- If the mobile is standing still and it can not see its target, then
		-- stroll around.
		else
			if (self.gma_action ~= "moving") then
				self.gma_action = "moving"
				gma.ApplyAnimation(self, "moving")
				gma.PlaySound(self, "moving")
			end

			gma.Move(self)
			self.gma_counter_change_direction = self.gma_direction_change_interval

		end
	end
end


-- Stop the mobile.
gma.Stop = function(self)
	if (self.gma_action ~= "standing") then
		self.gma_action = "standing"
		gma.ApplyAnimation(self, "standing")
		gma.PlaySound(self, "standing")
	end

	self.object:set_velocity({x = 0, y = 0, z = 0})
end


-- Check whether if the mobile is stuck.
gma.CheckIfStuck = function(self)
	if (self.gma_action == "moving")
	or (self.gma_action == "moving_fast")
	or (self.gma_action == "jumping")
	or (self.gma_action == "attacking_moving")
	then

		local velocity = self.object:get_velocity()

		if (velocity.x == 0)
		and (velocity.y == 0)
		and (velocity.z == 0)
		then
			--print(self.gma_mobile_id .. " is stuck.")
			return true

		else
			return false
			-- print(self.gma_mobile_id .. " is not stuck.")

		end
	end
end


-- Used to record if a mobile has fallen.
gma.FallCheck = function(self)
	local velocity = self.object:get_velocity()

	if (velocity.y < -1.0) then
		self.gma_fall_speed = velocity.y
	end
end
