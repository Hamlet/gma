--[[

	Copyright © 2018 Hamlet <hamlatmesehub@riseup.net>
	Copyright © 2016, 2018 azekillDIABLO, taikedz, Lone_Wolf

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


gma.OnActivateDefault = function(self, staticdata, dtime_s)

	self.gma_friendly_group = "prova"

	self.object:set_properties({gma_friendly_group = self.gma_friendly_group})

	-- Store the current position for later comparisons.
	self.gma_current_position = minetest.pos_to_string(self.object:get_pos(), 1)

	-- Assign a random unique identification code.
	gma.RandomID(self)

	-- Assign the hit points.
	gma.RandomHealth(self)

	-- Assign the armor level.
	gma.RandomArmor(self)

	-- Assign the damage level.
	gma.RandomDamage(self)

	-- Set the nametag.
	--gma.UpdateNametag(self)

	-- Reset movement and animation.
	gma.Stop(self)

	-- Apply the "standing still" animation, if possible.
	if (self.gma_animations_table ~= nil) then
		gma.ApplyAnimation(self, "standing")
	end

	-- Choose a random texture, if possible.
	if (self.textures ~= nil) and (#self.textures > 1)	then
		gma.ApplyRandomTexture(self)
	end
end
