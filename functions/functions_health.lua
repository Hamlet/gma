--[[

	Copyright © 2018 Hamlet <hamlatmesehub@riseup.net>
	Copyright © 2016, 2018 azekillDIABLO, taikedz, Lone_Wolf

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Health points related
--

-- Used to chose a random value between hp_min and hp_max.
gma.RandomHealth = function(self)
	if (self.gma_hp_min ~= self.hp_max) then
		-- Store for later use.
		self.gma_initial_hp = math.random(self.gma_hp_min, self.hp_max)
		-- Apply the chosen value.
		self.object:set_hp(self.gma_initial_hp)
	else
		self.gma_initial_hp = self.hp_max
		self.object:set_hp(self.gma_initial_hp)
	end
end


-- Used to determine whether if a mobile has to be injuried
-- due to fall damage.
gma.FallDamageCheck = function(self)
	local velocity = self.object:get_velocity()

	-- If the mobile isn't falling anymore.
	if (velocity.y > self.gma_fall_speed) then
		local damage_modifier = gma.RoundFloat(self.gma_fall_damage_modifier)

		local fall_speed = gma.RoundFloat(-self.gma_fall_speed)

		self.gma_fall_speed = 0.0
		local current_hp = self.object:get_hp()

		local damage = (current_hp / damage_modifier) * fall_speed
		damage = gma.RoundFloat(damage)
		damage = damage * damage
		--local message = " Current HP: " .. current_hp ..
		--	" Calculated damage: " .. damage

		if (damage > 0) then
			--print("Fall speed: " .. gma.RoundFloat(fall_speed) .. message)
			self.object:set_hp(self.object:get_hp() - damage)
		end
	end
end


-- Used to determine whether if a mobile has to be injuried
-- due to fire or lava damage.
gma.BurnCheck = function(self)
	--local function_time_start = os.clock()

	local burning = false

	-- If the mobile has no hit points left then remove it.
	if (self.object:get_hp() <= 0) then
		self.object:remove()

	else
		local mobile_position = self.object:get_pos()

		-- Mobile's torax, head, level.
		local current_upper_node = minetest.get_node(mobile_position)
		current_upper_node = current_upper_node.name
		local u_is_fire = minetest.get_item_group(current_upper_node, "igniter")
		local u_is_lava = minetest.get_item_group(current_upper_node, "lava")

		-- Feet level.
		local current_ground_node = minetest.get_node({
			z = mobile_position.z,
			y = mobile_position.y - 1,
			x = mobile_position.x
		})
		current_ground_node = current_ground_node.name
		local g_is_fire = minetest.get_item_group(current_ground_node, "igniter")
		local g_is_lava = minetest.get_item_group(current_ground_node, "lava")


		-- Check the head level node.
		if (u_is_fire ~= 0) and (u_is_lava == 0) then
			local fire_damage = (self.gma_initial_hp / 5)
			burning = true
			self.object:set_hp(self.object:get_hp() - fire_damage)
		end

		if (u_is_lava ~= 0) then
			local lava_damage = (self.gma_initial_hp / 2.5)
			burning = true
			self.object:set_hp(self.object:get_hp() - lava_damage)
		end


		-- Check the feet level node.

		if (g_is_fire ~= 0) and (g_is_lava == 0) then
			local fire_damage = (self.gma_initial_hp / 5)
			burning = true
			self.object:set_hp(self.object:get_hp() - fire_damage)
		end

		if (g_is_lava ~= 0) then
			local lava_damage = (self.gma_initial_hp / 2.5)
			burning = true
			self.object:set_hp(self.object:get_hp() - lava_damage)
		end
	end


	if (burning == true) then
		if (self.gma_state_b ~= "burning") then
			self.gma_state_b = "burning"
		end

	else
		if (self.gma_state_b ~= "normal") then
			self.gma_state_b = "normal"
		end

	end

--[[
	local function_time_end = (os.clock() - function_time_start) * 1000
	print("G.M.A. - Fire-Lava Damage Check took " ..
		string.format("%.2f", function_time_end) .. "ms")
--]]
end


-- Used to determine whether if a mobile has to be injuried
-- due to water damage.
gma.HydroCheck = function(self)
	--local function_time_start = os.clock()

	local hydro_damage = false

	-- If the mobile has no hit points left then remove it.
	if (self.object:get_hp() <= 0) then
		self.object:remove()

	else
		local mobile_position = self.object:get_pos()

		-- Mobile's torax, head, level.
		local current_upper_node = minetest.get_node(mobile_position)
		current_upper_node = current_upper_node.name
		local u_is_water = minetest.get_item_group(current_upper_node, "water")

		-- Feet level.
		local current_ground_node = minetest.get_node({
			z = mobile_position.z,
			y = mobile_position.y - 1,
			x = mobile_position.x
		})
		current_ground_node = current_ground_node.name
		local g_is_water = minetest.get_item_group(current_ground_node, "water")


		-- Check the head level node.
		if (u_is_water ~= 0) then
			local water_damage = (self.gma_initial_hp / 2.5)
			hydro_damage = true
			self.object:set_hp(self.object:get_hp() - water_damage)
		end

		-- Check the feet level node.

		if (g_is_water ~= 0) then
			local water_damage = (self.gma_initial_hp / 2.5)
			hydro_damage = true
			self.object:set_hp(self.object:get_hp() - water_damage)
		end
	end


	if (hydro_damage == true) then
		if (self.gma_state_c ~= "hydro_damage") then
			self.gma_state_c = "hydro_damage"
		end

	else
		if (self.gma_state_c ~= "normal") then
			self.gma_state_c = "normal"
		end

	end

--[[
	local function_time_end = (os.clock() - function_time_start) * 1000
	print("G.M.A. - Water Damage Check took " ..
		string.format("%.2f", function_time_end) .. "ms")
--]]
end


-- Used to determine whether if a mobile has to be injuried
-- due to light or darkness damage.
gma.LightLevelCheck = function(self)

	--local function_time_start = os.clock()

	local light_damage = false

	-- If the mobile has no hit points left then remove it.
	if (self.object:get_hp() <= 0) then
		self.object:remove()

	else
		local current_position = self.object:get_pos()
		local current_light_level = minetest.get_node_light(current_position)

		-- Check whether if to apply light damage (not darkness damage)
		if (self.gma_stand_light_min ~= -1)
		and (self.gma_stand_light_max ~= -1)
		then
			if (current_light_level >= self.gma_stand_light_min)
			and (current_light_level <= self.gma_stand_light_max)
			then
				light_damage = false

			else
				local damage = self.gma_light_damage

				if (damage == nil) then
					damage = (self.gma_initial_hp / 10)
				end

				light_damage = true
				self.object:set_hp(self.object:get_hp() - damage)
				--print(self.object:get_hp())
			end

		-- Check whether if to apply darkness damage
		else
			if (current_light_level >= 3) then
				light_damage = false

			else
				local damage = self.gma_light_damage

				if (damage == nil) then
					damage = (self.gma_initial_hp / 10)
				end

				light_damage = true
				self.object:set_hp(self.object:get_hp() - damage)

			end
		end
	end

	if (light_damage == true) then
		if (self.gma_state_d ~= "light_damage") then
			self.gma_state_d = "light_damage"
		end

	else
		if (self.gma_state_d ~= "normal") then
			self.gma_state_d = "normal"
		end

	end

--[[
	local function_time_end = (os.clock() - function_time_start) * 1000
	print("G.M.A. - Light Damage Check took " ..
		string.format("%.2f", function_time_end) .. "ms")
--]]
end


-- Used to determine whether if a mobile is injuried.
-- Returns true if injuried, false if not.
gma.HealthLevelCheck = function(self)
	if (self.object:get_hp() ~= self.gma_initial_hp) then
		return true
	else
		return false
	end
end


-- Used to check whether if mobiles can recover health.
gma.AllowHealthRecovery = function(self)
	if (self.gma_state_a ~= "suffocating")
	and (self.gma_state_b ~= "burning")
	and (self.gma_state_c ~= "hydro_damage")
	and (self.gma_state_d ~= "light_damage")
	and (self.gma_action ~= "attacking")
	and (self.gma_action ~= "attacking_moving")
	then
		return true
	else
		return false
	end
end


-- Used to slowly regenerate the mobiles' health.
gma.RecoverHealth = function(self)
	--print(self.gma_mobile_id .. " recovering health.")
	local recoverable_points = (self.gma_initial_hp / 20)

	if (recoverable_points < 1) then
		recoverable_points = 1
		--print("Recoverable points set to: " .. recoverable_points)
	end

	self.object:set_hp(self.object:get_hp() + recoverable_points)
	--print(self.object:get_hp() .. "/" .. self.gma_initial_hp)
	--gma.UpdateNametag(self)
end


--
-- Breath points related
--

-- Used to determine whether if the mobile is drowning.
-- If the mobile can't breath, breath points are subtracted.
-- If the mobile can't breath and no breath points are left then
-- hit points are subtracted.
gma.CanBreath = function(self)

	--local function_time_start = os.clock()

	local breathes_in = self.gma_breathes_in
	local mobile_position = self.object:get_pos()
	local current_node = minetest.get_node(mobile_position)
	current_node = current_node.name


	--
	-- DROWNING CHECK
	--
	if (breathes_in == "air") then
		-- Suffocation occurs if not into air
		-- doesn't occurs if into nodes belonging to
		-- "group:stair", "group:flora", "group:grass" and "group:leaves"
		if (current_node ~= "air")
		and (minetest.get_item_group(current_node, "stair") == 0)
		and (minetest.get_item_group(current_node, "flora") == 0)
		and (minetest.get_item_group(current_node, "grass") == 0)
		and (minetest.get_item_group(current_node, "leaves") == 0)
		then
			if (self.gma_state_a ~= "suffocating") then
				self.gma_state_a = "suffocating"
				--print(self.gma_mobile_id .. " - suffocating (no air)")
			end

		else
			if (self.gma_state_a ~= "normal") then
				self.gma_state_a = "normal"
				--print(self.gma_mobile_id .. " - breathing (air)")
			end
		end

	elseif (breathes_in == "water") then
		current_node = minetest.get_item_group(current_node, "water")

		-- If the current node does not belong to "group:water"
		if (current_node == 0) then
			if (self.gma_state_a ~= "suffocating") then
				self.gma_state_a = "suffocating"
				--print(self.gma_mobile_id .. " - suffocating (no water)")
			end

		else
			if (self.gma_state_a ~= "normal") then
				self.gma_state_a = "normal"
				--print(self.gma_mobile_id .. " - breathing (water)")
			end
		end
	end


	--
	-- BREATH POINTS or HIT POINTS subtraction
	--

	if (self.gma_state_a == "suffocating") then
		if (self.gma_breath_points > 0) then
			self.gma_breath_points = (self.gma_breath_points - 1)
			--print(self.gma_mobile_id .. " - B.P.: " .. self.gma_breath_points)

		else
			local suffocation_damage = (self.gma_initial_hp / 20)
			self.object:set_hp(self.object:get_hp() - suffocation_damage)
			--print(self.gma_mobile_id .. " - H.P.: " .. self.object:get_hp())

		end
	end

--[[
	local function_time_end = (os.clock() - function_time_start) * 1000
	print("G.M.A. - Suffocation Check took " ..
		string.format("%.2f", function_time_end) .. "ms")
--]]
end


-- Used to determine whether if a mobile has lost breath points.
gma.BreathLevelCheck = function(self)
	if (self.gma_breath_points < 10) then
		return true
	else
		return false
	end
end


-- Used to regenerate the mobile's breath points.
gma.RecoverBreath = function(self)
	self.gma_breath_points = (self.gma_breath_points + 1)
	--print(self.gma_mobile_id .. " - recovering breath")
end
