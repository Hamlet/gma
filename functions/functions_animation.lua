--[[

	Copyright © 2018 Hamlet <hamlatmesehub@riseup.net>
	Copyright © 2016, 2018 azekillDIABLO, taikedz, Lone_Wolf

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Used to check whether if an animation is already being played.
-- Boolean.
gma.AnimationCheck = function(self, animation_number)
	if (self.gma_current_animation == nil) then
		self.gma_current_animation = 1
	end

	if (self.gma_current_animation == animation_number) then
		return true
	else
		return false
	end
end


-- Mobile's animation
gma.ApplyAnimation = function(self, action)
	-- for backward compatibility: if the existing entities have been
	-- spawned without the "animations" specification in the entity
	-- definition, the game will not crash.
	if (self.gma_animations_table ~= nil) then
		local animations_table = self.gma_animations_table
		-- fetch the "animations" specification

		local standing = animations_table["standing"]
		-- fetch the values for the "standing" animation
		-- i.e. the mob stands still

		local sitting = animations_table["sitting"]
		-- fetch the values for the "standing" animation
		-- i.e. the mob stands still

		local laying = animations_table["laying"]
		-- fetch the values for the "standing" animation
		-- i.e. the mob stands still

		local moving = animations_table["moving"]
		-- fetch the values for the "moving" animation
		-- i.e. the mob walks, swims or flies

		local moving_fast = animations_table["moving_fast"]
		-- fetch the values for the "moving_fast" animation
		-- same as the previous, but for running or similar

		local jumping = animations_table["jumping"]
		-- fetch the values for the "jumping" animation

		local attacking = animations_table["attacking"]
		-- fetch the values for the "attacking" animation
		-- i.e. the mob bites, punches, digs, etc.

		local attacking_moving = animations_table["attacking_moving"]
		-- fetch the values for the "attacking while moving" animation
		-- i.e. the mob bites, punches, digs, etc. while moving

		if (action ~= nil) then
		-- when calling the function you must specify which action
		-- is being done, else the function will abort.
			if (action == "standing") then
				self.object:set_animation(
					standing[1],
					standing["frame_speed"],
					standing["frame_blend"],
					standing["frame_loop"]
				)

				self.gma_current_animation = 1

			elseif (action == "sitting") then
				self.object:set_animation(
					sitting[1],
					sitting["frame_speed"],
					sitting["frame_blend"],
					sitting["frame_loop"]
				)

				self.gma_current_animation = 2

			elseif (action == "laying") then
				self.object:set_animation(
					laying[1],
					laying["frame_speed"],
					laying["frame_blend"],
					laying["frame_loop"]
				)

				self.gma_current_animation = 3

			elseif (action == "moving") then
				self.object:set_animation(
					moving[1],
					moving["frame_speed"],
					moving["frame_blend"],
					moving["frame_loop"]
				)

				self.gma_current_animation = 4

			elseif (action == "moving_fast") then
				self.object:set_animation(
					moving_fast[1],
					moving_fast["frame_speed"],
					moving_fast["frame_blend"],
					moving_fast["frame_loop"]
				)

				self.gma_current_animation = 5

			elseif (action == "jumping") then
				self.object:set_animation(
					jumping[1],
					jumping["frame_speed"],
					jumping["frame_blend"],
					jumping["frame_loop"]
				)

				self.gma_current_animation = 6

			elseif (action == "attacking") then
				self.object:set_animation(
					attacking[1],
					attacking["frame_speed"],
					attacking["frame_blend"],
					attacking["frame_loop"]
				)

				self.gma_current_animation = 7

			elseif (action == "attacking_moving") then
				self.object:set_animation(
					attacking_moving[1],
					attacking_moving["frame_speed"],
					attacking_moving["frame_blend"],
					attacking_moving["frame_loop"]
				)

				self.gma_current_animation = 8

			else
				print("H.M.M.: action \"" .. action ..
					"\" not supported or typed wrong.")
			end

		else
			print("H.M.M.: action's value is nil.")

		end
	end
end
