--[[

	Copyright © 2018 Hamlet <hamlatmesehub@riseup.net>
	Copyright © 2016, 2018 azekillDIABLO, taikedz, Lone_Wolf

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Used for various purposes.
gma.Boolean = function()
	if (math.random(0, 1) == 0) then
		return false
	else
		return true
	end
end


-- Perform an action on a chance basis.
gma.ChanceCheck = function(number)
	local chance = number

	-- Force a default value if it has been entered an out of
	-- range one.
	if (chance == nil)
	or (chance < 1)
	or (chance > 100)
	then
		chance = 50
	end

	-- If chance = 100 then allow it.
	if (chance == 100) then
		return true
	end

	local number = math.random(1, 100)

	-- E.g. chance = 10
	-- If number == 1,2,3,...,10 then allow an action, else don't.
	if (number <= chance) then
		return true
	else
		return false
	end
end


-- Used for various purposes.
gma.RandomTableElement = function(table_name)
	return table_name[math.random(1, #table_name)]
end


-- Used to apply random textures
gma.ApplyRandomTexture = function(self)
	self.textures = gma.RandomTableElement(self.textures)
	self.object:set_properties({textures = self.textures})
end


-- Used to check whether if there is enough space to spawn mobiles.
-- Arguments: chosen pos, mobile's height, required node (e.g. {"air"})
gma.CheckForSpace = function(position, mobile_height, required_node)

	local y_axis = position.y + 1
	local z_axis = 0
	local x_axis = 0

	local test_result = true

	for n = 1, mobile_height do

		y_axis = y_axis + 1

		for n = -mobile_height, mobile_height do
			z_axis = position.z + n

			for n = -mobile_height, mobile_height do
				x_axis = position.x + n

				local node_to_check = {z = z_axis, y = y_axis, x = x_axis}
				local node_name = minetest.get_node(node_to_check).name

				if (node_name ~= required_node[1])
				and (minetest.get_item_group(node_name, "flora") == 0)
				and (minetest.get_item_group(node_name, "grass") == 0)
				then
					--print(node_name)
					test_result = false
					--print("Space check failed.")
				end
			end
		end
	end

	return test_result
end


-- Used to check whether if mobiles can be spawned.
-- "aoc": Active Object Count
gma.AllowSpawning = function(pos, aoc, aoc_wider, spawning_definition)

	--local function_time_start = os.clock()

	local checks_passed = false
	local aoc_passed = false
	local aoc_wider_passed = false
	local light_min_passed = false
	local light_max_passed = false
	local height_min_passed = false
	local height_max_passed = false
	local time_min_passed = false
	local time_max_passed = false
	local min_space_passed = false

	local detected_light_level = minetest.get_node_light({
			z = pos.z,
			y = (pos.y + 1),
			x = pos.x
		}, nil)

	local detected_height = (pos.y + 1)

	local detected_time = minetest.get_timeofday() * 24000

	-- Active Object Count check
	if (aoc < spawning_definition.f) then
		aoc_passed = true
	end

	-- Active Object Count wider check
	if (aoc_wider < spawning_definition.g) then
		aoc_wider_passed = true
	end

	-- Min light level check
	if (detected_light_level >= spawning_definition.h) then
		light_min_passed = true
	end

	-- Max light level check
	if (detected_light_level <= spawning_definition.i) then
		light_max_passed = true
	end

	-- Min height check
	if (detected_height >= spawning_definition.l) then
		height_min_passed = true
	end

	-- Max height check
	if (detected_height <= spawning_definition.m) then
		height_max_passed = true
	end

	-- Min time check
	if (detected_time >= spawning_definition.n) then
		time_min_passed = true
	end

	-- Max time check
	if (detected_time <= spawning_definition.o) then
		time_max_passed = true
	end

	-- Check if all tests have been succesfully passed.
	if  (aoc_passed == true)
	and (aoc_wider_passed == true)
	and (light_min_passed == true)
	and (light_max_passed == true)
	and (height_min_passed == true)
	and (height_max_passed == true)
	and (time_min_passed == true)
	and (time_max_passed == true)
	then

		checks_passed = true
	end

--[[
	local function_time_end = (os.clock() - function_time_start) * 1000
	print("G.M.A. - Spawn's checks took " ..
		string.format("%.2f", function_time_end) .. "ms")
--]]

	return checks_passed
end


-- Used to give an unique ID to mobiles.
gma.RandomID = function(self)

	local letters_table = {
		"a", "b", "c", "d", "e",
		"f", "g", "h", "i", "j",
		"k", "l", "m", "n", "o",
		"p", "q", "r", "s", "t",
		"u", "v", "w", "x", "y",
		"z"
	}

	local id_code = "Mob " .. letters_table[math.random(1, 26)] .. "-"
		.. math.random(1, 99) .. "-" .. math.random(1, 99)

	self.gma_mobile_id = id_code
	self.nametag = self.gma_mobile_id

	self.object:set_properties({nametag = self.nametag})
	--print(self.gma_mobile_id)
end


-- Used to assign a random armor level.
gma.RandomArmor = function(self)
	local armor_level = math.random(self.gma_armor_min, self.gma_armor_max)

	self.object:set_armor_groups({fleshy = armor_level})
end


-- Used to assign a random damage level.
gma.RandomDamage = function(self)
	local damage_level = math.random(self.gma_damage_min, self.gma_damage_max)

	self.gma_damage_dealt = damage_level
end


-- Used to format the mobiles' nametags
gma.UpdateNametag = function(self)
	self.object:set_nametag_attributes({
		color = "lightgrey",
		text = "H.P.: " .. self.object:get_hp() .. "/" .. self.gma_initial_hp
			.. "\nB.P.: " .. self.gma_breath_points,
	})
end


-- Used to check whether if a target is within a mobile's detection range.
gma.CheckIfIntoRange = function(pos1, pos2, range)
	local distance = vector.distance(pos1, pos2)

	if (distance <= range) then
		return true
	else
		return false
	end
end


-- Used to check whether if there is a direct line of sight
-- between two points.
gma.CheckLineOfSight = function(pos1, pos2, stepsize, offset)
	if (offset == nil) then
		--[[
		print("Mobile pos (line of sight): " .. minetest.pos_to_string(pos1, 1))
		print("Target pos (line of sight): " .. minetest.pos_to_string(pos2, 1))
		print("Sight offset: " .. tostring(offset))
		--]]
		if (minetest.line_of_sight(pos1, pos2, stepsize) == true) then
			return true
		else
			return false
		end

	else
		local mob_pos = pos1
		local foe_pos = pos2
		mob_pos.y = mob_pos.y + offset
		foe_pos.y = mob_pos.y + offset

		--[[
		print("Mobile pos (line of sight): " ..
			minetest.pos_to_string(mob_pos, 1))

		print("Target pos (line of sight): " ..
			minetest.pos_to_string(foe_pos, 1))

		print("Sight offset: " .. tostring(offset))
		--]]

		if (minetest.line_of_sight(mob_pos, foe_pos, stepsize) == true) then
			return true
		else
			return false
		end
	end
end


-- Used to round up float numbers.
gma.RoundFloat = function(float)
	local rounded_number = string.format("%.0f", float)
	rounded_number = tonumber(rounded_number)

	return rounded_number
end


-- Used for various purposes.
gma.RealTimeToInGameTime = function(seconds)
	local time_speed = tonumber(minetest.settings:get("time_speed"))
	local in_game_day_length = nil
	local in_game_seconds = nil

	if (time_speed == nil) then
		-- Minetest Game's default time speed = 72
		in_game_day_length = 1200 -- i.e. 20 minutes (1200 / 60)

	elseif (time_speed == 0) then
		-- If time is set to 0 mobiles will behave as if it is set to 1.
		in_game_day_length = 86400 -- i.e. 24 hours (86400 / 60) / 60

	else
		in_game_day_length = 86400 / time_speed
	end

	in_game_seconds = (in_game_day_length * seconds) / 86400

	return in_game_seconds
end
