### Generic Mobiles API
![Generic Mobiles API's screenshot](screenshot.png)  
**_Manages mobiles' registration and behaviour._**

**Version:** 0.1.1  
**Source code's license:** [EUPL v1.2][1] or later  
**Media (Textures, Models, Sounds) license:** [CC BY-SA v4.0 International][2] or later

**Dependencies:** none  
**Supported:** default


### Installation

Unzip the archive, rename the folder to gma and place it in  
../minetest/mods/

If you only want this to be used in a single world, place it in  
../minetest/worlds/WORLD_NAME/worldmods/

GNU+Linux - If you use a system-wide installation place it in  
~/.minetest/mods/

For further information or help see:  
https://wiki.minetest.net/Help:Installing_Mods


[1]: https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863
[2]: https://creativecommons.org/licenses/by-sa/4.0/
