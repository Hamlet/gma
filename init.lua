--[[

	Copyright © 2018-2019 Hamlet <hamlatmesehub@riseup.net>
	Copyright © 2016, 2018 azekillDIABLO, taikedz, Lone_Wolf

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Global API's namespace
--

gma = {}


--
-- Functions' files
--

dofile(minetest.get_modpath("gma") .. "/functions/functions_animation.lua")
dofile(minetest.get_modpath("gma") .. "/functions/functions_combat.lua")
dofile(minetest.get_modpath("gma") .. "/functions/functions_generic.lua")
dofile(minetest.get_modpath("gma") .. "/functions/functions_health.lua")
dofile(minetest.get_modpath("gma") .. "/functions/functions_movement.lua")
dofile(minetest.get_modpath("gma") .. "/functions/functions_on_activate.lua")
dofile(minetest.get_modpath("gma") .. "/functions/functions_on_step.lua")
dofile(minetest.get_modpath("gma") .. "/functions/functions_sound.lua")
dofile(minetest.get_modpath("gma") .. "/functions/functions_timers.lua")


--
-- Mobiles registration
--

--[[
	Based on:
	../minetest/doc/lua_api.txt at line 4019 (v0.4.17.1)
	../minetest/doc/lua_api.txt at line 4053 (v0.4.17.1)
	Variables prefixed by "gma" are custom, e.g. "gma_hp_min"
--]]

gma.mobile = function(mobile_name, mobile_definition)
	minetest.register_entity(":" .. mobile_name, {
		hp_max = mobile_definition.p
			or 20,

		physical = mobile_definition.b
			or true,

		collide_with_objects = mobile_definition.c
			or true,

		weight = mobile_definition.o
			or 5,

		collisionbox = mobile_definition.d
			or	{-0.35, -0.35, -0.35, 0.35, 0.35, 0.35},

		visual = mobile_definition.e
			or "mesh",

		visual_size = mobile_definition.f
			or {x = 1, y = 1},

		mesh = mobile_definition.g
			or nil,

		textures = mobile_definition.h
			or nil,

		colors = mobile_definition.i
			or nil,

		is_visible = mobile_definition.a
			or true,

		makes_footstep_sound = mobile_definition.u
			or true,

		automatic_rotate = mobile_definition.k
			or false,

		stepheight = mobile_definition.n
			or nil,

		automatic_face_movement_dir = mobile_definition.l
			or false,

		automatic_face_movement_max_rotation_per_sec = mobile_definition.m
			or -1.0,

		backface_culling = mobile_definition.j
			or false,

		nametag = mobile_definition.r
			or nil,

		nametag_color = mobile_definition.s
			or "white",

		infotext = mobile_definition.t
			or "Mobile",

		gma_mobile_id = nil,

		gma_friendly_group = mobile_definition.ra
			or nil,

		gma_hostile_group = mobile_definition.rb
			or nil,

		gma_feared_group = mobile_definition.rc
			or nil,

		gma_temper = mobile_definition.rd
			or 1,

		gma_team_fight = mobile_definition.re
			or false,

		gma_scan = mobile_definition.rf
			or false,

		gma_scan_interval = mobile_definition.rf1
			or 5.0,

		gma_hp_min = mobile_definition.q
			or 20,

		gma_mobility = mobile_definition.oa
			or 50,

		gma_direction_change_interval = mobile_definition.ob
			or 15.0,

		gma_stand_light_min = mobile_definition.qa
			or 0,

		gma_stand_light_max = mobile_definition.qb
			or 3,

		gma_light_damage = mobile_definition.qc
			or nil,

		gma_sounds_table = mobile_definition.v
			or nil,

		gma_animations_table = mobile_definition.w
			or nil,

		gma_life_time = mobile_definition.x
			or 1200,

		gma_range_detect = mobile_definition.xa
			or 14,

		gma_range_melee = mobile_definition.xb
			or 4,

		gma_sight_offset = mobile_definition.xc
			or nil,

		gma_speed = mobile_definition.xd
			or 1,

		gma_fast_speed = mobile_definition.xe
			or 4,

		gma_armor_min = mobile_definition.xf
			or 100,

		gma_armor_max = mobile_definition.xg
			or 100,

		gma_damage_min = mobile_definition.xh
			or 1,

		gma_damage_max = mobile_definition.xi
			or 1,

		gma_damage_variable = mobile_definition.xj
			or false,

		gma_breathes_in = mobile_definition.ya
			or "air",

		gma_suffocation = mobile_definition.yb
			or true,

		gma_flaws_fire = mobile_definition.za
			or true,

		gma_flaws_water = mobile_definition.zb
			or false,

		gma_flaws_light = mobile_definition.zc
			or false,

		gma_flaws_fall = mobile_definition.zd
			or true,

		gma_fall_damage_modifier = mobile_definition.ze
			or 50,

		gma_breath_points = 10,

		gma_damage_dealt = 1,

		gma_initial_hp = 20,

		gma_counter_movement = 0.0,

		gma_counter_burn_check = 0.01,

		gma_counter_hydro_check = 0.01,

		gma_counter_light_check = 0.01,

		gma_counter_change_direction = 0.01,

		gma_counter_gravity_check = 0.01,

		gma_counter_fall_check = 0.01,

		gma_counter_fall_damage_check = 0.01,

		gma_counter_recover_breath_check = 0.01,

		gma_counter_recover_health_check = 0.01,

		gma_counter_stuck_check = 0.01,

		gma_counter_suffocation_check = 0.01,

		gma_counter_melee_attack = 0.01,

		gma_fall_speed = 0.0,

		gma_action = "standing",

		gma_current_animation = 1,

		gma_current_sound = nil,

		gma_foe = nil,

		gma_state_a = "normal",
		-- Air/Water. "normal" or "suffocating"

		gma_state_b = "normal",
		-- Burning. "normal" or "burning"

		gma_state_c = "normal",
		-- Water damage. "normal" or "hydro_damage"

		gma_state_d = "normal",
		-- Light damage. "normal" or "light_damage"

		gma_state_e = "normal",
		-- Combat. "normal" or "engaged"

		-- Function "on activate"
		-- Arguments: (self, staticdata, dtime_s)
		-- ../minetest/doc/lua_api.txt at line 3924 (v0.4.17.1)
		on_activate = mobile_definition.f_a
			or function(self, staticdata, dtime_s)
				gma.OnActivateDefault(self, staticdata, dtime_s)
			end,

		-- Function "on step"
		-- Arguments: (self, dtime)
		-- ../minetest/doc/lua_api.txt at line 3928 (v0.4.17.1)
		on_step = mobile_definition.f_b
			or function(self, dtime)
				gma.OnStepDefault(self, dtime)
			end,

		-- Function "on punch"
		-- Arguments:
		-- (self, puncher, time_from_last_punch, tool_capabilities, dir)
		-- ../minetest/doc/lua_api.txt at line 3932 (v0.4.17.1)
		on_punch = mobile_definition.f_c
			or function(self, puncher, time_from_last_punch,
				tool_capabilities, dir)
			gma.OnPunchDefault(self, puncher, time_from_last_punch,
				tool_capabilities, dir)
			end,

		-- Function "on rightclick"
		-- Arguments: (self, clicker)
		-- ../minetest/doc/lua_api.txt at line 3941 (v0.4.17.1)
		on_rightclick = mobile_definition.f_d
			or function(self, clicker)
				--print(dump(self.object:get_luaentity()))
				local pos = self.object:get_pos()
				local nearby_mobiles = minetest.get_objects_inside_radius(pos, 14)

				for n = 1, #nearby_mobiles do
					if (nearby_mobiles[n]:is_player() == false) then
						local mobile_properties = nearby_mobiles[n]:get_luaentity()
						local mobile_group = mobile_properties.gma_friendly_group
						print(mobile_group)
						--print(dump(mobile_properties.gma_friendly_group))
					end
				end
			end,

		-- Function "get staticdata"
		-- Arguments: (self)
		-- ../minetest/doc/lua_api.txt at line 3942 (v0.4.17.1)
		get_staticdata = mobile_definition.f_e
			or nil
	})
end


--
-- Mobiles spawner (Active Block Modifier)
--

-- ../minetest/doc/lua_api.txt at line 4073 (v0.4.17.1)

gma.abm = function(mobile_name, spawning_definition)
	minetest.register_abm({
		label = "H.M.M. mobiles spawner - " .. mobile_name,
		nodenames = spawning_definition.a
			or {"group:crumbly"},

		neighbors = spawning_definition.b
			or {"air"},

		interval = spawning_definition.c
			or 45,

		chance = spawning_definition.d
			or 7500,

		catch_up = spawning_definition.e
			or false,

		gma_active_object_count = spawning_definition.f
			or 1,

		gma_active_object_count_wider = spawning_definition.g
			or 2,

		gma_light_min = spawning_definition.h
			or 4,

		gma_light_max = spawning_definition.i
			or 15,

		gma_height_min = spawning_definition.l
			or 2,

		gma_height_max = spawning_definition.m
			or 240,

		gma_time_min = spawning_definition.n
			or 4700.0,

		gma_time_max = spawning_definition.o
			or 19250.0,

		gma_required_space = spawning_definition.p
			or nil,

		action = function(pos, node, active_object_count,
			active_object_count_wider)
		--
			if (gma.AllowSpawning(pos, active_object_count,
				active_object_count_wider,	spawning_definition) == true)
			then

				-- If the mobile's definition does not require more than
				-- one node, then it will be spawned one node above the
				-- chosen node.
				-- E.g. into an "air" node above a "dirt with grass" node.
				if (spawning_definition.p == nil) then
					--print("Normal spawning.")
						minetest.add_entity({
							z = pos.z,
							y = (pos.y + 1),
							x = pos.x
							},
							mobile_name
						)

				else
					if	(gma.CheckForSpace(pos, spawning_definition.p,
						spawning_definition.b or "air") == true)
					then
						--print("Spawning after checking space.")

						-- If the mobile's definition does require more than
						-- one node, then it will be spawned X nodes above the
						-- chosen node.
						-- E.g. into an "air" node 2 nodes higher than a
						-- "dirt with grass" node.
						minetest.add_entity({
							z = pos.z,
							y = (pos.y + spawning_definition.p),
							x = pos.x
							},
							mobile_name
						)

					end
				end
			end
			--
		end
	})
end


--
-- Mobiles spawner (Item)
--

-- ../minetest/doc/lua_api.txt at line 4110 (v0.4.17.1)

gma.spawner = function(mobile_name, node_infotext, node_texture, height_offset)
	minetest.register_craftitem(":" .. mobile_name .. "_spawner", {
		description = node_infotext,
		inventory_image = node_texture,
		wield_image = node_texture,

		on_use = function(itemstack, user, pointed_thing)
			local pos = pointed_thing.above
			local position = {}

			if (height_offset ~= nil) then
				position = {
					x = pos.x,
					y = (pos.y + height_offset),
					z = pos.z
				}

			else
				position = pos

			end

			minetest.add_entity(position, mobile_name)
			itemstack:take_item();
			return itemstack
		end,
	})
end


--
-- Ascii Art (http://patorjk.com/software/taag/)
--

print("\n")
print("\t  .g8\"\"\"bgd    `7MMM.     ,MMF'         db         ")
print("\t.dP'     `M      MMMb    dPMM          ;MM:        ")
print("\tdM'       `      M YM   ,M MM         ,V^MM.       ")
print("\tMM               M  Mb  M' MM        ,M  `MM       ")
print("\tMM.    `7MMF'    M  YM.P'  MM        AbmmmqMA      ")
print("\t`Mb.     MM ,,   M  `YM'   MM  ,,   A'     VML  ,, ")
print("\t  `\"bmmmdPY db .JML. `'  .JMML.db .AMA.   .AMMA.db ")
print("\n")


--
-- Minetest engine debug logging
--

if (minetest.settings:get("debug_log_level") == nil)
or (minetest.settings:get("debug_log_level") == "action")
or (minetest.settings:get("debug_log_level") == "info")
or (minetest.settings:get("debug_log_level") == "verbose")
then
	minetest.log("action", "[Mod] Generic Mobiles A.P.I. [v0.1.1] loaded.")
end
